## Test environments

## Resubmission v1.6.0

* Ubuntu 22.04, R 4.2.1 
* win-builder (devel and release) 
* Windows Server 2022, R-oldrel, 32/64 bit 
* Windows Server 2022, R-devel, 64 bit 
* macOS 10.13.6 High Sierra, R-release, CRAN's setup (macos-highsierra-release-cran) 
* R-hub Fedora Linux, R-devel, GCC (fedora-gcc-devel) 
* R-hub Ubuntu Linux 20.04.1 LTS, R-release, GCC 
* R-hub Debian Linux, R-devel, GCC 

## R CMD check results

After a first re-submission and an email exchange with Uwe Ligges, 
I removed the backslash that caused the problem, and resubmited right after.

There were no ERROR nor WARNING nor NOTE on Ubuntu 22.04 R 4.2.1.

There were no ERRORs or WARNINGs and 1 or 2 NOTEs on 'win-builder' on R devel 
and R-release, respectively :
* R-devel: https://win-builder.r-project.org/0k6220ITyNrU/00check.log 
* R-release: https://win-builder.r-project.org/4JY3e550htHX/00check.log 

NOTEs R-release and R-devel:
* checking CRAN incoming feasibility ... NOTE
Maintainer: 'Paul Savary <psavary@protonmail.com>'

Found the following (possibly) invalid URLs:
  URL: https://besjournals.onlinelibrary.wiley.com/doi/abs/10.1111/2041-210X.13530
    From: inst/CITATION
    Status: 503
    Message: Service Unavailable

Found the following (possibly) invalid DOIs:
  DOI: 10.1111/j.1365-294X.2004.02177.x
    From: DESCRIPTION
    Status: Service Unavailable
    Message: 503
  DOI: 10.1111/mec.14059
    From: DESCRIPTION
    Status: Service Unavailable
    Message: 503

* checking Rd files ... [5s] NOTE
checkRd: (-1) gen_graph_indep.Rd:126: Escaped LaTeX specials: \&

- The DOI are correct and the URL is valid.
- The note due LaTeX specials is due to the references and does not affect
how the package runs. FIXED after an email exchange

There were no ERRORs or WARNINGs but one or two NOTEs on:

* R-hub Windows Server 2022, R-oldrel, 32/64 bit
https://builder.r-hub.io/status/graph4lg_1.8.0.tar.gz-657e14e7a52e43f09db61a980f5e791a 

* R-hub Windows Server 2022, R-devel, 64-bit
https://builder.r-hub.io/status/graph4lg_1.8.0.tar.gz-fc78f171697c49a4852838c54116bc4b 


With the command: rhub::check(env_vars=c(R_COMPILE_AND_INSTALL_PACKAGES = "always")) in all the cases

* checking dependencies in R code ... NOTE

(Without more information only for Windows Server 2022, R-oldrel, 32/64 bit)

* checking dependencies in R code ... NOTE
 All declared Imports should be used.
   Namespace in Imports field not imported from: 'Rdpack'

- The previous version was submitted because CRAN asked for moving these packages in Imports, previously in Suggests

* checking Rd files ... NOTE
checkRd: (-1) gen_graph_indep.Rd:126: Escaped LaTeX specials: \&

- Same as above, due to references and does not affect the package functioning
FIXED after an email exchange

There was one WARNING and one NOTE with:

* R-hub macOS 10.13.6 High Sierra, R-release, CRAN's setup 
https://builder.r-hub.io/status/graph4lg_1.8.0.tar.gz-82a8d244e0404ac4b805d4748fcf093c 

* checking for missing documentation entries ... WARNING
Warning: package ‘ade4’ was built under R version 4.1.2
All user-level objects in a package should have documentation entries.
See chapter ‘Writing R documentation files’ in the ‘Writing R
Extensions’ manual.

- ade4 is not a dependency of graph4lg and there is not much I can do about it
I think.

* checking dependencies in R code ... NOTE
Namespace in Imports field not imported from: ‘Rdpack’
  All declared Imports should be used.
  
- The previous version was submitted because CRAN asked for moving these packages in Imports, previously in Suggests

There was no ERRORs or WARNINGs but two NOTE and also a PREPERROR with:

* R-hub Fedora Linux, R-devel, GCC (fedora-gcc-devel)
https://builder.r-hub.io/status/graph4lg_1.8.0.tar.gz-5d01f3a4be9d498799da4b92c5310460 

* R-hub Ubuntu Linux 20.04.1 LTS, R-release, GCC 
https://builder.r-hub.io/status/graph4lg_1.8.0.tar.gz-ca9bc56d7a944fe39a2d47fbdae43ec1 

* R-hub Debian Linux, R-devel, GCC 
https://builder.r-hub.io/status/graph4lg_1.8.0.tar.gz-beb7aece292c4e89be116defa1f88b49 

R CMD check finished with two NOTEs:
* checking dependencies in R code ... NOTE
 Namespace in Imports field not imported from: ‘Rdpack’
All declared Imports should be used.

* checking Rd files ... NOTE
checkRd: (-1) gen_graph_indep.Rd:126: Escaped LaTeX specials: \&
FIXED after an email exchange

PREPERROR:
Error: No such container: No such container: graph4lg_1.8.0.tar.gz-ca9bc56d7a944fe39a2d47fbdae43ec1-3

In all cases, it does not seem to be a problem related to the package, 
but rather a problem due to the checking platform on Linux.

The last lines are:
Build step 'Send files or execute commands over SSH' changed build result to SUCCESS
Pinging https://builder.r-hub.io/build/SUCCESS/graph4lg_1.8.0.tar.gz-ca9bc56d7a944fe39a2d47fbdae43ec1/2023-01-28T22:46:32Z
{"status":"ok"}
Finished: SUCCESS

## Downstream dependencies

There are currently no downstream dependencies for this package.
