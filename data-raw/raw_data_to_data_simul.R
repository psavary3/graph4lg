#### Import de donnees, mise en forme, conversions #######

library(pegas)
library(adegenet)
library(rgdal)

setwd(dir="D:/psavary/Mes documents/THESE/Analyses/package_cours/graph4lg")
dir_def<-"D:/psavary/Mes documents/THESE/Analyses/package_cours/graph4lg/data-raw"

#pts <- rgdal::readOGR(dsn = dir_def, layer = "patches")

pts <- sf::as_Spatial(sf::st_read(dsn = dir_def, layer = "patches"))

pts <- data.frame(sp::coordinates(pts))
pts$ID <- as.character(1:50)
names(pts) <- c("x", "y", "ID")
pts <- pts[, c("ID", "x", "y")]


pts_pop_simul <- pts


#data<-read.table(file="data-raw/gpop_51_sim22_01_25.txt")

data_simul_genind <- genepop_to_genind(path = "data-raw/gpop_51_sim22_01_25.txt",
                                       n.loci = 20)

usethis::use_data(pts_pop_simul)
usethis::use_data(data_simul_genind)


